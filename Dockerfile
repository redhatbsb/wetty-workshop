FROM node:0.10.38
#FROM registry.access.redhat.com/rhscl/nodejs-4-rhel7
MAINTAINER Luciano e Gustavo <lscorsin@redhat.com> 

ADD . /app
WORKDIR /app
RUN npm install && apt-get update && apt-get install -y vim && useradd -u 1000060000 -l -d /home/workshop -m -s /bin/bash workshop && echo 'workshop:workshop' | chpasswd && mkdir /home/workshop/.ssh/ && chmod 700 /home/workshop/.ssh 
COPY ssh/ /home/workshop/.ssh/
RUN chmod 0600 /home/workshop/.ssh/id_rsa && chmod 0600 /home/workshop/.ssh/id_rsa.pub && chown -R workshop:workshop /home/workshop/.ssh

EXPOSE 3000
USER workshop
ENTRYPOINT node app.js -p 3000 --sshhost $openshift
